function ls_mask = get_ls_mask(model)

c = CMIP5.CMIP5_metadata;

ls_mask = ncread([c.data_loc '/sftlf/sftlf_fx_' model '_historical_r0i0p0.nc'],'sftlf');

end
