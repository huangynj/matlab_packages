function var_mean = area_mean(var,lon,lat,varargin)
%
% Function to calculate the area mean for a variable given on a lat-lon grid
% Lat-lon grid must be staggered with respect to the var points
% area_mean treates nan points as missing.
%
% INPUTS:
%	var(Nlon,Nlat)    = variable on lat-on grid
%	lon(Nlon+1)     = lon grid (staggered)
%	lat(Nlat+1)     = lat grid (staggered)
%       weight(Nlon,Nlat) = weighting function (optional)
%
% OUTPUTS:
%	var_mean	  = mean of the variable over lat-lon grid
%

weight = ones(size(var));
if nargin >= 4; weight = varargin{1}; end


% If data is already zonal mean
if length(lon)<=1
    lon = [0 1];
    weight = weight(:)';
end

lon = repmat(lon(:),size(var(1,:)));
lat = repmat(lat(:)',size(var(:,1)));

dlon = diff(lon,1,1);
dlat = diff(lat,1,2);
coslat = cosd( (lat(:,1:end-1)+lat(:,2:end))./2 );

var_mean = nansum(nansum(coslat.*var.*weight.*dlat.*dlon))./nansum(nansum(coslat.*weight.*dlat.*dlon));



end


