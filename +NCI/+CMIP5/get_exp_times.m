function [time_bnds] = get_exp_times(exper)


c = NCI.CMIP5.CMIP5_metadata;


yeari = c.(exper).years(1);
yearf = c.(exper).years(2);



time_bnds(1) = datenum(yeari,1,1);
time_bnds(2) = datenum(yearf+1,1,1);
 

end
