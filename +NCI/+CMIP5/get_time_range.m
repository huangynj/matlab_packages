function [time_beg,time_end] = get_time_range(file);

%
% Function to get the range of times in a CMIP file from its name
%
% CMIP files are named like this:
% ta_day_IPSL-CM5B-LR_historical_r1i1p1_19000101-19091231.nc
%
% so it is easy to get the time range by the file name, 
% saving having to read the file to determine the relevant times

if findstr('day',lower(file))

   beg_str = file(end-19:end-12);
   end_str = file(end-10:end-3);

   time_beg = datenum(beg_str,'yyyymmdd');
   time_end = datenum(end_str,'yyyymmdd');

elseif findstr('mon',lower(file))>0

   beg_str = file(end-15:end-10);
   end_str = file(end-8:end-3);

   time_beg = datenum(beg_str,'yyyymm');
   time_end = eomdate(datenum(end_str,'yyyymm'));

elseif ~isempty(findstr('6hr',lower(file)))

   beg_str = file(end-23:end-14);
   end_str = file(end-12:end-3);


   time_beg = datenum(beg_str,'yyyymmddhh');
   time_end = datenum(end_str,'yyyymmddhh');

elseif ~isempty(findstr('3hr',lower(file)))

   if ~isempty(findstr('GFDL',file)) | ~isempty(findstr('inmcm4',file))
     beg_str = file(end-23:end-14);
     end_str = file(end-12:end-3);


     time_beg = datenum(beg_str,'yyyymmddhh');
     time_end = datenum(end_str,'yyyymmddhh');

   else
     beg_str = file(end-27:end-16);
     end_str = file(end-14:end-3);


     time_beg = datenum(beg_str,'yyyymmddhhMM');
     time_end = datenum(end_str,'yyyymmddhhMM');

   end


else
  error(['time error in get_time_range:' file])
end



