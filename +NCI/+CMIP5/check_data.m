function check_data(structure,variables,mod_levs)
 


 for i = 1:length(variables)
   if mod_levs(i)
      times{i} =  structure.(variables{i}).num_times_modlev;
   else
      times{i} =  structure.(variables{i}).num_times;
   end
   if ~( times{i} == times{1} )
      error(['variable size ' variables{i} ' does not match with ' variables{1}])
   end

 end  

end 

