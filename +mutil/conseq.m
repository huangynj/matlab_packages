function z = conseq(x)
% Calculates the number of consequtive true values after each element in a
% vector
% z = conseq(x)
% i.e. if x = [1 0 1 1 1 0 1 1 0 0 0 1 0 1]
%         z = [1 0 3 2 1 0 2 1 0 0 0 1 0 1]
% For matricies conseq acts along the rows of the matrix.
% NOTE x MUST CONTAIN ONLY ones and zeros.
%
% 
%
  m = size(x,1);
  y = [reshape([zeros(1,m);x.'],[],1);0];
  z = y;
  p = find(~y);
  d = 1-diff(p);
  
  % if need count ups rather than count downs use this
  %y(p) = [0;d];
  %y = reshape(cumsum(y(1:end-1)),[],m).';
  %y(:,1) = [];
  
  z(p) = [d;0];
  z = reshape(cumsum(-z(1:end-1)),[],m).';
  z(:,end) = [];
 
 
end
