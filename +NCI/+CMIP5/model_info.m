function model = model_info(model_name)

  this_dir = fileparts(mfilename('fullpath'));
  info_file =  [this_dir '/file_info/' model_name '.mat'];

  load(info_file)

end
