function [time_vec,I_vec,files_vec,files,varargout] = get_time_vector(structure,variable,varargin)

mm = NCI.CMIP5.CMIP5_metadata;

 % Get variables on model levels or on pressure levels?
 model_levs = 0;
 if nargin > 2; model_levs = varargin{1}; end

 time_vec = [];
 greg_vec = [];
 files_vec= [];
 I_vec= [];


 time =  structure.(variable).time;
 
 if model_levs
    in =  structure.(variable).times_in_modlev;
 else
    in =  structure.(variable).times_in;
 end
 files =  structure.(variable).files;



 for i = 1:length(time)
     
      ini  = in{i};
      timei = time{i};
      time_vec = [time_vec; timei(ini)];
      I_vec = [I_vec; ini];
      files_vec = [files_vec; i.*ones(size(timei(ini)))];

      if isfield(structure.(variable), 'greg_time')
         gregi = structure.(variable).greg_time{i};
         greg_vec = [greg_vec; gregi(ini,:)];
      end
 end

if nargout > 4; varargout{1} = greg_vec; end
