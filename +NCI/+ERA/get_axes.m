function  [lat,lon,p] = get_axes(file)

disp(file)
 f = netcdf.open(file,'nowrite');

 varid = netcdf.inqVarID(f,'lat');
 lat    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'lon');
 lon    = netcdf.getVar(f,varid);

 p= 0;

 try
   varid = netcdf.inqVarID(f,'plev');
   p    = netcdf.getVar(f,varid);
 catch

   disp('could not find pressure, looking for lev')
   try
     varid = netcdf.inqVarID(f,'lev');
     p    = netcdf.getVar(f,varid);
   catch
     disp('No vertical coord')
   end

 end

 netcdf.close(f)



end
