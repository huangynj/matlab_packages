function  [lat,lon,p,varargout] = get_axes(file,varargin)
% Static axes calculation

% Get the model name if required
model = 'none';
if nargin > 1; model = varargin{1}; end


 % Get the standard coordinates, lon, lat, and the level variable
 f = netcdf.open(file,'nowrite');

 varid = netcdf.inqVarID(f,'lat');
 lat    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'lon');
 lon    = netcdf.getVar(f,varid);

 % level variable is sometimes called plev, other times lev
 % some files have no level variable 
 try
   varid = netcdf.inqVarID(f,'plev');
   p    = netcdf.getVar(f,varid);
 catch

   disp('could not find pressure, looking for lev')
   try
     varid = netcdf.inqVarID(f,'lev');
     p    = netcdf.getVar(f,varid);
   catch
     disp('No vertical coord')
     p= 0;
   end

 end


 %% Now get the model level variables if required
 if nargout > 3
  if ~isempty(findstr(model,'GFDL')) | ~isempty(findstr(model,'BNU')) | ~isempty(findstr(model,'MIROC')) | ~isempty(findstr(model,'Nor'));

   a = double(ncread(file,'a'));
   varargout{1} = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);

   b = double(ncread(file,'b'));
   varargout{2} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

   varargout{3} = double(ncread(file,'p0'));

   a = double(ncread(file,'a_bnds'));
   a = [a(1,:) a(2,end)]';
   varargout{4} = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);

   b = double(ncread(file,'b_bnds'));
   b = [b(1,:) b(2,end)]';
   varargout{5} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);




  elseif ~isempty(findstr(model,'ACCESS'))

   % Get the vertical coordinate variables
   orog = double(ncread(file,'orog'));
   a = double(ncread(file,'lev'));
   b = double(ncread(file,'b'));
  
   % Expand
   orog_mat = repmat(orog,[1 1 length(a)]);
   a = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);
   b = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);
 
   % get the geopotential height at scalar levels
   z = a + b.*orog_mat;

   % Get the interface levels
   a = double(ncread(file,'lev_bnds'));
   a = [a(1,:) a(2,end)]';

   b = double(ncread(file,'b_bnds'));
   b = [b(1,:) b(2,end)]';

   orog_mat = repmat(orog,[1 1 length(a)]);
   a = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);
   b = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

   % Interface levels
   z_i = a + b.*orog_mat;
   dz = diff(z_i,1,3);

   varargout{1} = z;
   varargout{2} = z_i;
   varargout{3} = dz;
   varargout{4} = '';
   varargout{5} = '';

  elseif ~isempty(findstr(model,'IPSL'))
   
   a = double(ncread(file,'ap'));
   varargout{1} = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);

   b = double(ncread(file,'b'));
   varargout{2} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

   varargout{3} = '';

   a = double(ncread(file,'ap_bnds'));
   a = [a(1,:) a(2,end)]';
   varargout{4} = permute(repmat(a,[1 length(lon) length(lat)]),[2 3 1]);

   b = double(ncread(file,'b_bnds'));
   b = [b(1,:) b(2,end)]';
   varargout{5} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

  elseif ~isempty(findstr(model,'FGOALS'))

   varargout{1} = '';
   b = double(ncread(file,'lev'));
   varargout{2} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

   c = double(ncread(file,'ptop'));
   varargout{3} = c;

   b = double(ncread(file,'lev_bnds'));
   b = [b(1,:) b(2,end)]';
   varargout{4} = permute(repmat(b,[1 length(lon) length(lat)]),[2 3 1]);

   varargout{5} = '';

  else 

    error('model vertical coord error')

  end



 netcdf.close(f)



end
