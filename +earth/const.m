function c = const
%
% Provides some constants relevant for Earth



    % Earth radius (m)
    c.Re        = 6.3662e6;


    % Rotation rate (s^-1)
    c.Omega = 7.2921150e-5;
