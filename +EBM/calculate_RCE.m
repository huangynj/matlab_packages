function Ts = calculate_RCE(varargin)
%
%
% Calculate the Surface temperature required for RCE assuming
%   1) Gray radiation scheme
%   2) Moist adiabat up to 200 K
%
%

S_in = 0.25.*1360;          % Solar constant W/m^2
surface_albedo =0.38;
tau_0 = 3.5;                % long-wave optical depth of atmosphere (linear in pressure)
solar_tau_0 = 0.22;  
RH = 0.8;
Tguess = 288;

if nargin >= 1; S_in = varargin{1}; end
if nargin >= 2; tau_0 = varargin{2}; end
if nargin >= 3; surface_albedo = varargin{3}; end
if nargin >= 4; solar_tau_0 = varargin{4}; end
if nargin >= 5; RH = varargin{5}; end
if nargin >= 6; Tguess = varargin{6}; end

% Load FMS constants
c = atm.load_constants('FMS');


% Create pressure matrices
ps = 100000;
p = 99500:-1000:500;

% First guess surface temperature
Ts = Tguess;

% Sensitivity of radiation to surface temperature at each iteration
% Units: K/(W/m^2)
Rad_sensitivity = 0.2;
Net_TOA = 1;
i = 1;

while abs(Net_TOA) > 0.01




% Surface-air quantities
Ta = Ts-1;
pa = ps;

% Get surface humidity
ea = RH.*atm.e_sat(Ta,c.type);
ra = c.eps.*ea./(pa-ea);

% Calculate moist adiabatic temperature profile
[T,rv,rl,ri,T_rho] = atm.calculate_adiabat(Ta,ra,pa,p,1,c.type,c.ice,c.deltaT);
if Ts>200
   T(T<200) = 200;
else
    T(T<Ts) = Ts;
end

% Calculate the radiative fluxes
p_rad = p(end:-1:1);
T_rad = T(end:-1:1);
[LW_up,LW_down,SW_up,SW_down,p_half] = EBM.calculate_gray_radiation(p_rad,ps,T_rad,Ts,S_in,tau_0,surface_albedo,solar_tau_0);

Net_TOA = -(LW_up(1) + SW_up(1)) + SW_down(1);

Ts_new = Ts + Net_TOA.*Rad_sensitivity;
dT = Ts_new - Ts;
Ts = Ts_new;

%disp(['iter: ' num2str(i) ', delta T = ' num2str(dT) ' K, F_TOA = ' num2str(Net_TOA) ' W/m^2, T_s = ' num2str(Ts) ' K'])
i = i+1;

end



end






