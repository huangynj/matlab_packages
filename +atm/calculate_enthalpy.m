function h = calculate_enthalpy(T,rv,rl,ri,varargin)
%
% Function to calculate the enthalpy
%
% s = calculate_enthalpy(T,r {type,ice,deltaT})
%
% Calculates entropy based on pressure (p) Temperature (T) and 

% either:
%         vapor mixing ratio (r) and given values of liquid and and solid water (rl,ri) 
% or:
%         using total water content (r) with mixed-phase to find
%         the amount of liquid and solid.
%


    %% Define some constants
    c = atm.load_constants(varargin{1:end});

        
    % Load optional inputs
    if isempty(ri)
       p = rl;
       r_t = rv;
       [rv,rl,ri] = atm.saturation_adjustment(p,T,r_t,varargin{1:end});
    else
       r_t = rv+rl+ri;
    end
 

    % calculate enthalpy per unit dry air mass:
    
    % Calculate Enthalpy
    hd = c.cp.*(T - c.T0);
    hv = c.cpv.*(T - c.T0) + c.Lv0;
    hl = c.cpl.*(T - c.T0);
    hi = c.cpi.*(T - c.T0) + c.Lv0 - c.Ls0;
    
    %h = (1-qi+ql+qv).*hd + qv.*hv + ql.*hl + qi.*hi;
    h = hd + rv.*hv + rl.*hl + ri.*hi;

    
    
    
