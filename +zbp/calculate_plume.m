function [T_rho,T,p,rv,rsat,rl,ri,z,h,T_env,r_env,h_env,ent] = calculate_plume(T_base,rt_base,p_base,entrain,RH,varargin)
%ZERO_BUOYANCY_PLUME calculate zero-buoyancy plume thermodynamic profile
%
% This is the zero-buoyancy plume model used in Singh & O'Gorman (2013).
% The model solves for the temperature and humidity profile of an
% entraining plume that is neutrally buoyant with respect to its
% environment given a base temperature and humidity and an
% environmental relative humidity.
%
% %% USAGE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Simply running the script with no input arguments will solve the model
% with some example parameters and produce a few plots.
%
% The full calling syntax is:
%
% [T_rho,T,p,rv,rsat,rl,ri,z,h,T_env,r_env,h_env,ent] = ...
%            calculate_plume(                ...
%                 T_base,rv_base,p_base,entrain,RH,     ...
%                [ent_type,z_bot,z_top,deltaz,gamma,type,ice,deltaT])
%
% ARGUMENTS:
%           T_base  :  base temperature of plume            (K)
%           rt_base :  base mixing ratio of plume 	    (kg/kg)
%           p_base  :  base pressure of plume               (Pa)
%           entrain :  entrainment parameter                (unitless)
%                        Dependent on "ent_type"
%                        entrainment = entrain/z            [default]
%                        entrainment = entrain/1000
%           RH      :  environmental relative humidity      (0-1)
%
% OPTIONAL ARGUMENTS:
%           ent_type:  type of entrainment profile                 ['invz']
%                        'invz' : entrainment = entrain/z
%                        'const': entrainment = entrain/1000
%           z_bot   :  base height of plume                 (m)    [50]
%           z_top   :  height to which plume is integrated  (m)    [15000]
%           deltaz  :  vertical grid-spacing                (m)    [50]
%
%           gamma   :  fraction of water that precipitates  (0-1)  [1]
%                       0 = no fallout
%                       1 = pseudo-adiabatic
%	    type    :  thermodynamics type 		(see package +atm)
%           ice     :  include ice in thermodynamics? 	(see package +atm)
%           deltaT  :  Mixed phase range (K)		(see package +atm)
%
%
% OUTPUTS:
%           T_rho   : Density temperature (plume and env)   (K)
%           T       : Temperature of plume                  (K)
%           p       : pressure            (plume and env)   (K)
%           rv      : water vapor mixing ratio of plume     (kg/kg)
%           rsat    : sat. mixing ratio of plume 	    (kg/kg)
%           rl      : liquid mixing ratio of plume   	    (kg/kg)
%           ri      : solid mixing ratio of plume 	    (kg/kg)
%           z       : height                                (z)
%           h       : plume moist static energy             (J/kg)
%           T_env   : Temperature of environment            (K)
%           r_env   : specific humidity of environment      (kg/kg)
%           h_env   : environment moist static energy       (J/kg)
%           ent     : entrainment rate                      (1/m)
%
%
%
% %% MODEL DEATILS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% The entraining plume model solves the equations
%
% dh/dz      = -epsilon (h   - h_env)
% dq_t/dz    = -epsilon (q_t - q_env)
% dlog(p)/dz = - g/(R_d T_rho)
%
% where h is the moist static energy, q_t is the total water mass fraction, p is 
% the pressure, and T_rho is the density temperature.
%
% The moist static energies are defined:
%
% h_d = c_pd (T -T0)  + gz
% h_v = c_pv (T -T0)  + gz + Lv0 q_v
% h_l = c_l  (T -T0)  + gz
% h_i = c_i  (T -T0)  + gz - Lf0 q_i
% h   = (1-q_t)h_d + h_v s_v + q_l h_l + q_i h_i
%
% The equations are integrated in height using Euler forward differences for
% the entrainment terms. Precipitation fallout is handled separately.
% After each Euler step a fraction \gamma of the increase in liquid and
% solid water in the plume is discarded, and the moist static energy
% adjusted consistently.
%
% The environmental properties are calculated by assuming the density of
% the plume and parcel are equal, and using the given environmental
% relative humidity.
%
% Full details are found in the supplementary information of
% Singh & O'Gorman (2013). The equations used are consistent with the
% parcel model of Romps & Kuang (2010) applied to a vertically oriented,
% steady flow in the limit of zero cloud buoyancy.
%
%
%
% %% References %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Romps, D.M. (2008). The Dry-Entropy Budget of a Moist Atmosphere.
% J. Atmos. Sci., 65, 3779-3799.
%
% Romps, D.M. & Kuang, Z (2010). Do Undiluted Convective Plumes Exist in
% the Upper Tropical Troposphere?. J. Atmos. Sci., 67, 468-484.
%
% Singh, M.S. & O'Gorman (2013). Influence of entrainment on the thermal
% stratification in simulations of radiative-convective equilibrium.
% Geophs. Res. Lett, 40, doi:10.1002/grl.50796.
%
% %% AUTHOR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Written by Martin S. Singh, 10 Jul 2013.
%            mssingh@mit.edu
%
% 
%
% %% UPDATE HISTORY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   16 Jul 2013: MS 
%       - Added argument checks
%       - Fixed bug in specifying total freezing temperature (T_ice)
%       - Cleaned up some typos in comments
%
%   28 Aug 2013: MS 
%       - updated reference to Singh & O'Gorman as article now published
%
%   19 Jan 2017: MS
%	- now implemented as matlab package
%	- requires +atm matlab package for flexible thermodynamics



%% Demonstration %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 0
    T_base  = 300;
    rt_base = 0.0155;
    p_base  = 100000;
    entrain = 0.5;
    RH      = 0.7;
end

%% Default Input arguments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants(varargin{6:end});

ent_type = 'invz' ; % Type of entrainment profile
                    % 'invz'  1/z profile;                entrainment_rate = entrain/z
                    % 'const' constant in z profile;      entrainment_rate = entrain/1000

z_base = 50;        % Height of plume initial condition    (m)
z_top = 15000;      % Height to which plume is integrated  (m)
deltaz = 50;        % Vertical grid spacing                (m)
c.gamma = 1;        % Fraction of condensation that falls out as precipitation

% Optional arguments
if nargin >= 6;  ent_type   = varargin{1}; end;
if nargin >= 7;  z_base     = varargin{2}; end;
if nargin >= 8;  z_top      = varargin{3}; end;
if nargin >= 9;  deltaz     = varargin{4}; end;
if nargin >= 10; c.gamma    = varargin{5}; end;



% Argument checks:
%              input    name       type      min    max
check_argument(T_base  ,'T_base'  ,'numeric',0     ,500   );
check_argument(rt_base ,'rt_base' ,'numeric',0     ,1     );
check_argument(p_base  ,'p_base'  ,'numeric',0     ,inf   );
check_argument(entrain ,'entrain' ,'numeric',0     ,inf   );
check_argument(RH      ,'RH'      ,'numeric',0     ,1     );
check_argument(ent_type,'ent_type','char'   ,0     ,500   );
check_argument(z_base  ,'z_base'  ,'numeric',0     ,z_top );
check_argument(z_top   ,'z_top'   ,'numeric',z_base,inf   );
check_argument(deltaz  ,'deltaz'  ,'numeric',0     ,inf   );
check_argument(c.gamma ,'gamma'   ,'numeric',0     ,1     );
check_argument(c.type  ,'type'    ,'char'   ,0     ,599   );
check_argument(c.ice   ,'ice'     ,'numeric',0     ,1     );
check_argument(c.deltaT,'deltaT'  ,'numeric',0     ,100   );



%% Height vector %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z       = z_base:deltaz:z_top;

% make sure z_top is a level
if abs(z(end)- z_top ) > 0.1;    z(end+1) = z_top; end

%% Entrainment profile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set entrainment rate as function of z
switch ent_type
    case 'invz'
        % Entrainment rate  m^-1
        ent     = min(1e-2,entrain./z);
    case 'const'
        % Entrainment rate  m^-1
        ent = 0.001.*entrain.*ones(size(z));
    otherwise
        error('unknown entrainment profile type')
end

%% Initialize verctors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p       = zeros(length(z),1);
logp    = zeros(length(z),1);

rv      = zeros(length(z),1);
rsat    = zeros(length(z),1);
rl      = zeros(length(z),1);
ri      = zeros(length(z),1);
rt      = zeros(length(z),1);

T       = zeros(length(z),1);
T_rho   = zeros(length(z),1);

h       = zeros(length(z),1);

T_env   = zeros(length(z),1);
r_env   = zeros(length(z),1);
h_env   = zeros(length(z),1);


%% Initial conditions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inputs
p(1)     = p_base;
T(1)     = T_base;
rt(1)    = rt_base;


% Determine if there is any liquid or solid water
[rv(1),rl(1),ri(1)] = atm.saturation_adjustment(p_base,T_base,rt_base,c.type,c.ice,c.deltaT);
if rl(1)>0 || ri(1) > 0; warning('PLUME:SuperSat','plume base super-saturated'); end

% Derived properties
logp(1)  = log(p(1));
h(1)     = atm.calculate_MSE(T(1),p(1),z(1),rt(1),rl(1),ri(1),c.type,c.ice,c.deltaT);

% Flag for LCL level
LCL = 0;


%% Integrate model upward %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(z)
    
    
    % Calculate plume density temperature
    T_rho(i) = T(i).*(1+rv(i)./c.eps)./(1+rt(i));
    
    % Calculate plume saturation specific humidity
    rsat(i) = atm.r_sat(T(i),p(i),c.type,c.ice,c.deltaT);
    
    
    % Calculate environment properties
    % Assume:
    %           1) equal density to plume
    %           2) Fixed relative humidity
    
    % Tv(env) = T_rho(plume)
    % Use root finding algorithm with initial guess
    T_env(i) = fzero(@(x) calculate_Tv(x,RH,p(i),c.type,c.ice,c.deltaT)-T_rho(i) ,T(i).*(1+0.61.*RH.*rv(i)));
    
    % Calculate environment humidity based on assumed relative humidity
    e_env = RH.*atm.e_sat(T_env(i),c.type,c.ice,c.deltaT);
    r_env(i) = c.eps.*(e_env./(p(i)-e_env));
    
    % Calculate environment moist static energy
    h_env(i) = atm.calculate_MSE(T_env(i),p(i),z(i),r_env(i),c.type,c.ice,c.deltaT);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if i<length(z)
        
        
        % No entrainment if unsaturated
        % Include LCL flag to prevent entrainment turning off above LCL
        if (rt(i)-rsat(i)) < 0 && LCL == 0;
            ent(i)=0;
        else
            LCL = 1;
        end
        
        % Step upward - simple Euler method

        % Moist static energy
        h(i+1) =    h(i) - ent(i).*( h(i) - h_env(i) ) .*( z(i+1)-z(i) );

        % total water mass fraction
        qti = rt(i)./(1 + rt(i));
        qei = r_env(i)./(1+r_env(i));
        qtip1 =   qti - ent(i).*( qti-qei )  .*( z(i+1)-z(i) );
        rt(i+1) = qtip1./(1-qtip1);

        % Log pressure
        logp(i+1) = logp(i) - c.g./(c.Rd.*T_rho(i))       .*( z(i+1)-z(i) );
        
        % Calculate pressure
        p(i+1)  = exp(logp(i+1));
        
        % Calculate Temperature via root finding algorithm
        T(i+1)  = fzero(@(x) atm.calculate_MSE(x,p(i+1),z(i+1),rt(i+1),c.type,c.ice,c.deltaT)-h(i+1) ,T(i));
       
        % Calculate humidity
        [rv(i+1),rl(i+1),ri(i+1)] = atm.saturation_adjustment(p(i+1),T(i+1),rt(i+1),c.type,c.ice,c.deltaT);
        
        
        % Rainfall fallout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if rt(i+1)-rv(i+1)>0 && c.gamma > 0;
            
            if c.gamma>=1 % Total fallout:
                
                %   1) Set q to its saturation value
                rt(i+1) = atm.r_sat(T(i+1),p(i+1),c.type,c.ice,c.deltaT);
                [rv(i+1),rl(i+1),ri(i+1)] = atm.saturation_adjustment(p(i+1),T(i+1),rt(i+1),c.type,c.ice,c.deltaT);
                
                %   2) Recalculate moist static energy
                h(i+1)  = atm.calculate_MSE(T(i+1),p(i+1),z(i+1),rt(i+1),c.type,c.ice,c.deltaT);
                
            else        % Partial fallout:
                
                % Calculate increase in liquid/solid water
                drls = (rl(i+1)+ri(i+1)-rl(i)-ri(i));
                
                % Calculate fallout
                rl(i+1),ri(i+1) = atm.simple_fallout(T(i+1),drls,rl(i+1),ri(i+1),c.gamma,c,type,c.ice,c.deltaT);
                rt(i+1) = rv(i+1)+rl(i+1)+ri(i+1); 

                % Adjust moist static energy
                h(i+1)  = atm.calculate_MSE(T(i+1),rt(i+1),p(i+1),z(i+1),c.type,c.ice,c.deltaT);
                
            end
            
        end
    end
end


if nargin ==0 % Demonstration plot
    figure;
    set(gcf,'defaultaxeslinewidth',1,'defaultaxesfontsize',10)
    
    % Left axis
    axes('position',[0.1 0.12 0.4 0.72]);
     plot(T_rho,z./1000,'r','linewidth',2)
     xlabel('T_\rho (K)')
     ylabel('z (km)')
     set(gca,'xtick',[200 250 300],'xlim',[180 310])
     title('(a) Density temperature')
     box off
    
    % Right axis
    axes('position',[0.53 0.12 0.4 0.72]);
     plot(h./1000,z./1000,'r','linewidth',2)
     hold on
     plot(h_env./1000,z./1000,'b','linewidth',2)
    
     title('(b) Moist static energy')
     xlabel('h (kJ/kg)')
     set(gca,'xtick',[40 50 60 70 80],'xlim',[50 80])
     set(gca,'yticklabel','')
     box off
     
     l = legend('plume','env');
     set(l,'box','off')
     
    suptitle(['RH_{env} = ' num2str(RH) ', \epsilon(z) = ' num2str(entrain) '/z'])
    
end


end



function Tv = calculate_Tv(T,RH,p,varargin)
% Function to calculate virtual temperature at a given relative humidity

c = atm.load_constants(varargin{1:end});


% Calculate mixing ratio
es = atm.e_sat(T);
qv = c.eps.*(RH.*es./(p-RH.*es.*(1-c.eps)));

% calculate virtual temperature
Tv = T.*(1+qv./c.eps-qv);

end





function check_argument(var,varname,type,varmin,varmax)
% Function to check arguments are correct


if ~isa(var,type);
    error(['Input: ' varname ' must be of type ' type]); 
end

if isnumeric(var)
    if var < varmin || var > varmax; 
        error(['Input: ' varname ' outside of bounds [' num2str(varmin) ',' num2str(varmax) ']']); 
    end
end

end
    
    
    
    
    
    
    



