function [T_rho,T,p,rv,rsat,rl,ri,z,h,T_env,r_env,h_env,hs_env,ent] = calculate_plume_spectrum(T_LCL,p_LCL,entrain,RH,eta,varargin)
%Test of calculation of plume spectrum from Zhou & Xie (2019).
%
% Incomplete and untested in various ways.
%
% 

% Lift from the LCL





%% Demonstration %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 0
    T_LCL  = 295;
    p_LCL  = 95000;
    entrain = 0.7;
    RH      = 0.7;
    eta = 1;
end

%% Default Input arguments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c = atm.load_constants(varargin{6:end});

z_LCL = 500;        % Height of plume initial condition    (m)
z_top = 15000;      % Height to which plume is integrated  (m)
deltaz = 50;        % Vertical grid spacing                (m)
c.gamma = 1;        % Fraction of condensation that falls out as precipitation

% Optional arguments
if nargin >= 7;  z_LCL     = varargin{1}; end;
if nargin >= 8;  z_top      = varargin{2}; end;
if nargin >= 9;  deltaz     = varargin{3}; end;
if nargin >= 10; c.gamma    = varargin{4}; end;



%% Height vector %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
z       = z_LCL:deltaz:z_top;

% make sure z_top is a level
if abs(z(end)- z_top ) > 0.1;    z(end+1) = z_top; end

%% Entrainment profile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Entrainment rate should be input as vector
% if not, set it to 1/z profile

z_t = 13000;
k = 4;
ent = entrain.*1e-3.*( (z_t - z)./(z_t - z_LCL) ).^k;
ent(z>z_t) = 0;


%if length(entrain)==1
%    ent = min(1e-2,entrain./z);
%end

%% Initialize verctors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
p       = zeros(length(z),1);
logp    = zeros(length(z),1);

rv      = zeros(length(z),1);
rsat    = zeros(length(z),1);
rl      = zeros(length(z),1);
ri      = zeros(length(z),1);
rt      = zeros(length(z),1);

T       = zeros(length(z),1);
T_rho   = zeros(length(z),1);

h       = zeros(length(z),1);

T_env   = zeros(length(z),1);
r_env   = zeros(length(z),1);
h_env   = zeros(length(z),1);
hs_env  = zeros(length(z),1);
dlogedz = zeros(length(z),1);

%% Initial conditions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Inputs
p(1)     = p_LCL;
T(1)     = T_LCL;
rl(1)    = 0;
ri(1)    = 0;
rv(1) = atm.r_sat(T_LCL,p_LCL,c.type,c.ice,c.deltaT);
rt(1) = rv(1);

% Derived properties
logp(1)  = log(p(1));
h(1)     = atm.calculate_MSE(T(1),p(1),z(1),rt(1),rl(1),ri(1),c.type,c.ice,c.deltaT);
hb = h(1);


dlogedz = 1./ent.*gradient(ent,z);
dlogedz(ent<1e-8) = 0;



%% Integrate model upward %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:length(z)
    
    
    % Calculate density temperature
    T_rho(i) = T(i).*(1+rv(i)./c.eps)./(1+rt(i));
    
    % Calculate saturation specific humidity
    rsat(i) = atm.r_sat(T(i),p(i),c.type,c.ice,c.deltaT);
    
    
    % Calculate environment properties
    % Assume:
    %           1) equal density to plume with zero buoyancy at current level
    %           2) Fixed relative humidity
    % Tv(env) = T_rho(plume)
    % Use root finding algorithm with initial guess
    T_env(i) = fzero(@(x) calculate_Tv(x,RH,p(i),c.type,c.ice,c.deltaT)-T_rho(i) ,T(i).*(1+0.61.*RH.*rv(i)));
    
    % Calculate environment humidity based on assumed relative humidity
    e_env = RH.*atm.e_sat(T_env(i),c.type,c.ice,c.deltaT);
    r_env(i) = c.eps.*(e_env./(p(i)-e_env));
    
    % Calculate environment moist static energy
    h_env(i) = atm.calculate_MSE(T_env(i),p(i),z(i),r_env(i),c.type,c.ice,c.deltaT);
    hs_env(i) = atm.calculate_MSE(T_env(i),p(i),z(i),'sat',c.type,c.ice,c.deltaT);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if i<length(z)
        
        
        % No entrainment if unsaturated
        % Include LCL flag to prevent entrainment turning off above LCL
        
        lambda = 1./(1+eta.*ent(i).*(z(i)-z_LCL)).*dlogedz(i);
        
        % Step upward - simple Euler method

        % Moist static energy
        h(i+1) =    h(i) + lambda.*(h(i) - hb)         .*( z(i+1)-z(i) ) ...
                         - ent(i).*( h(i) - h_env(i) ) .*( z(i+1)-z(i) );

        % total water mass fraction
        qti = rt(i)./(1 + rt(i));
        qei = r_env(i)./(1+r_env(i));
        qtip1 =   qti - ent(i).*( qti-qei )  .*( z(i+1)-z(i) );
        rt(i+1) = qtip1./(1-qtip1);

        % Log pressure
        logp(i+1) = logp(i) - c.g./(c.Rd.*T_rho(i))       .*( z(i+1)-z(i) );
        
        % Calculate pressure
        p(i+1)  = exp(logp(i+1));
        
        % Calculate Temperature via root finding algorithm
        T(i+1)  = fzero(@(x) atm.calculate_MSE(x,p(i+1),z(i+1),rt(i+1),c.type,c.ice,c.deltaT)-h(i+1) ,T(i));
       
        % Calculate humidity
        [rv(i+1),rl(i+1),ri(i+1)] = atm.saturation_adjustment(p(i+1),T(i+1),rt(i+1),c.type,c.ice,c.deltaT);
        
        
        % Rainfall fallout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if rt(i+1)-rv(i+1)>0 && c.gamma > 0;
            
            if c.gamma>=1 % Total fallout:
                
                %   1) Set q to its saturation value
                rt(i+1) = atm.r_sat(T(i+1),p(i+1),c.type,c.ice,c.deltaT);
                [rv(i+1),rl(i+1),ri(i+1)] = atm.saturation_adjustment(p(i+1),T(i+1),rt(i+1),c.type,c.ice,c.deltaT);
                
                %   2) Recalculate moist static energy
                h(i+1)  = atm.calculate_MSE(T(i+1),p(i+1),z(i+1),rt(i+1),c.type,c.ice,c.deltaT);
                
            else        % Partial fallout:
                
                % Calculate increase in liquid/solid water
                drls = (rl(i+1)+ri(i+1)-rl(i)-ri(i));
                
                % Calculate fallout
                rl(i+1),ri(i+1) = atm.simple_fallout(T(i+1),drls,rl(i+1),ri(i+1),c.gamma,c,type,c.ice,c.deltaT);
                rt(i+1) = rv(i+1)+rl(i+1)+ri(i+1); 

                % Adjust moist static energy
                h(i+1)  = atm.calculate_MSE(T(i+1),rt(i+1),p(i+1),z(i+1),c.type,c.ice,c.deltaT);
                
            end
            
        end
    end
end


if nargin ==0 % Demonstration plot
    fig.bfig('a5l');
    set(gcf,'defaultaxeslinewidth',1,'defaultaxesfontsize',10)
    
    % Left axis
    axes('position',[0.1 0.12 0.28 0.72]);
     plot(T_rho,z./1000,'r','linewidth',2)
     xlabel('T_\rho (K)')
     ylabel('z (km)')
     set(gca,'xtick',[200 250 300],'xlim',[180 310])
     title('(a) Density temperature')
     box off
    
    % Right axis
    axes('position',[0.4 0.12 0.28 0.72]);
     plot(h./1000,z./1000,'r','linewidth',2)
     hold on
     plot(h_env./1000,z./1000,'b','linewidth',2)
    
     title('(b) Moist static energy')
     xlabel('h (kJ/kg)')
     set(gca,'xtick',[40 50 60 70 80],'xlim',[50 80])
     set(gca,'yticklabel','')
     box off
     
     l = legend('plume','env');
     set(l,'box','off')
     
     axes('position',[0.7 0.12 0.28 0.72]);
    
     
    suptitle(['RH_{env} = ' num2str(RH) ', \epsilon(z) = ' num2str(entrain) '[(z_t-z)/(z_t-z_b)]^' num2str(k) ' '])
    
end


end



function Tv = calculate_Tv(T,RH,p,varargin)
% Function to calculate virtual temperature at a given relative humidity

c = atm.load_constants(varargin{1:end});


% Calculate mixing ratio
es = atm.e_sat(T);
qv = c.eps.*(RH.*es./(p-RH.*es.*(1-c.eps)));

% calculate virtual temperature
Tv = T.*(1+qv./c.eps-qv);

end





function check_argument(var,varname,type,varmin,varmax)
% Function to check arguments are correct


if ~isa(var,type);
    error(['Input: ' varname ' must be of type ' type]); 
end

if isnumeric(var)
    if var < varmin || var > varmax; 
        error(['Input: ' varname ' outside of bounds [' num2str(varmin) ',' num2str(varmax) ']']); 
    end
end

end
    
    
    
    
    
    
    



