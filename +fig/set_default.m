function set_default(varargin)
%
% Set some nice figure defaults to make things look good.
%

% FYI:
% One column figures in JAS are 8.1 cm wide
%

type = 'standard';
h    = groot;

if nargin >= 1; type = varargin{1}; end
if nargin >= 2; h = varargin{2}; end


set(h,'defaultaxestickdir','out');
set(h,'defaultAxesTickDirMode', 'manual');

set(h,'defaultcolorbartickdir','out');

set(h,'defaultaxesbox','off');
set(h,'defaultaxesfontname','helvetica');



switch type

 case 'standard'
% For looking at on the screen


    set(h,'defaultaxesfontsize',12);
    set(h,'defaultaxeslinewidth',1);
    set(h,'defaultlinelinewidth',1);


 case 'pres'
 % For powerpoint presentation slides
 
    set(h,'defaultaxesfontsize',16);
    set(h,'defaultcolorbarfontsize',18);
    set(h,'defaultaxeslinewidth',2);
    set(h,'defaultcolorbarlinewidth',2);
    set(h,'defaultlinelinewidth',2);
    set(h,'defaultaxesticklength',[0.015 0.015]);
    set(h,'defaultcolorbarticklength',[0.02]);


 case 'paper'
 % For documents like papers


    set(h,'defaultaxesfontsize',7);
    set(h,'defaultaxeslinewidth',0.5);
    set(h,'defaultlinelinewidth',0.5);

 otherwise 
    error('unknown figure settings')


end

