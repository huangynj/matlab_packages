function [files] = get_files(varargin)
%
% Function to list the data files availible for a given scenario, model and variable in CMIP5
%
% [files] = experiment(simulation,model,freq,variable)
%
%
%
%




c = NCI.ERA.ERA_metadata;

% Defaults
model = 'ERA_INT';
variable = 'ta';


if nargin >=1; model = varargin{1}; end
if nargin >=2; variable = varargin{2}; end


dir_in = [c.data_loc '/' model '/' model '/'];
info = dir([dir_in '/*_' variable '_*.nc' ]); 

files = {info(:).name};


for i = 1:length(files)
   files{i} = [dir_in '/' files{i}];
end







