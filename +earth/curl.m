function curlF = div(Fx,Fy,lon,lat,varargin);
% Calculate the horizontal curl of a vector field defined on a sphere
%
%
% curlF = curl(Fx,Fy,lon,lat);
%
% Vector field (Fx,Fy) defined at points given by lon,lat.
%
% div(F) = 1/Rcos(lat) d/dlat ( F_lat Rcos(lat) ) + 1/Rcos(lat) d/dlon F_lon
%
% Assume that x component of vecotr is zero at pole
%

%% Load the earth constants
c = earth.const;

%% Make axis matrices
[lon_mat,lat_mat] = mutil.mesh(lon,lat);

% Get vertical size
Nz = size(Fx,3);

if Nz>1
   lon_mat = repmat(lon_mat,[1 1 Nz]);
   lat_mat = repmat(lat_mat,[1 1 Nz]);
end

%% Define axes at interface points
loni = cat(1,lon_mat(end,:,:)-360,lon_mat,360+lon_mat(1,:,:));
loni = (loni(2:end,:,:)+loni(1:end-1,:,:))./2;
dlon = diff(loni,1,1).*pi./180;

lati = cat( 2 , -90.*ones(size(lat_mat(:,1,:))),lat_mat , 90.*ones(size(lat_mat(:,1,:))) );
lati = (lati(:,2:end,:)+lati(:,1:end-1,:))./2;
dlat = diff(lati,1,2).*pi()./180;

%% Define F on the interface points
Fyi = cat(1,Fy(end,:,:),Fy,Fy(1,:,:));
Fyi = (Fyi(2:end,:,:)+Fyi(1:end-1,:,:))./2;

Fxi = cat( 2,zeros(size(Fx(:,1,:))),Fx,zeros(size(Fx(:,end,:))) );
Fxi = (Fxi(:,2:end,:)+Fxi(:,1:end-1,:))./2;


curlF = 1./(c.Re.*cosd(lat_mat)) .* ( - diff(Fxi.*cosd(lati),1,2)./dlat + diff(Fyi,1,1)./dlon );

