function [T,rv,rl,ri] = invert_enthalpy(h,rt,p,T,varargin)
%
% Invert enthalpy to give temperature given total water content and pressure
% Uses an iteration procedure. Should converge in a few iterations
%
% [T,rv,rl,ri] = invert_enthalpy(h,rt,p,[T,type,ice,deltaT])
%


c = atm.load_constants(varargin{1:end});


T_err_max = 1;
i_iter = 0;

% Calculate the breakdown of water species based on a guessed temperature
[rv,~,~] = atm.saturation_adjustment(p,T,rt,varargin{1:end});

% Calculate the enthalpy given our guess of the temperature
h_guess = atm.calculate_enthalpy(T,rt,p,[],varargin{1:end});

while T_err_max > 0.0001
 i_iter = i_iter+1;


 % Improve the guess using a linear approximation
 % ISSUE: the tangent approximation has a disconinuity at saturation
 dhdT = c.cp.*ones(size(T));
 sat = rt>rv;
 
 dhdT(sat) = c.cp + (c.Lv0.^2./(c.Rv.*T(sat).^2)).*rv(sat);
 
 
 % Calculate a new temperature guess
 T_new = T + (h - h_guess)./dhdT;

 % Calculate the breakdown of water species based on the new temperature
 [rv,~,~] = atm.saturation_adjustment(p,T_new,rt,varargin{1:end});

 h_guess = atm.calculate_enthalpy(T_new,rt,p,[],varargin{1:end});
  
 
 % Get the error for this guess (convert to temperature)
 T_err= abs((h-h_guess)./c.cp);
 [T_err_max,Ierr] = max(T_err(:));
 T = T_new;

 if i_iter == 10
    % If we get to 10 iterations, we are probably in a loop between two values of T far from the correct one.
    % Try to salvage a solution with a last ditch perturbation to the temperature field.
    T(T_err>0.1) = T(T_err>0.1)+5;
 elseif i_iter > 20 & i_iter <= 30
    % Now we are really likely to be not converged. Print some output to warn the user
    disp(['iter: ' num2str(i_iter) ', error = ' num2str(T_err_max) ' K'])
    disp(['iter: ' num2str(i_iter) ', (I,h,T,rt,rv) = ' num2str([Ierr,h(Ierr),T(Ierr),rt(Ierr),rv(Ierr),p(Ierr)])])
 elseif i_iter > 30
     % Pull the plug
    error('enthalpy inversion did not converge. This may be because the input data is corrupt.')
 end
 
end
 
[rv,rl,ri] = atm.saturation_adjustment(p,T,rt,varargin{1:end});
