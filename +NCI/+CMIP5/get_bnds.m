function  [lat_bnds,lon_bnds] = get_axes(file)
% Static axes calculation



 % Get the standard coordinates, lon, lat, and the level variable
 f = netcdf.open(file,'nowrite');

 varid = netcdf.inqVarID(f,'lat_bnds');
 lat_bnds    = netcdf.getVar(f,varid);

 varid = netcdf.inqVarID(f,'lon_bnds');
 lon_bnds    = netcdf.getVar(f,varid);
 

 netcdf.close(f)



end
