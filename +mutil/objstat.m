function [Ncells,cell_area,varargout] = objstat(R)
%
% [Ncells,cell_area[,L,C]] = objstat(R)
%
% counts the number of connected regions in a binary matrix R
% and gives the number of regions and their average size (in pixels)
%
% Can account for doubly periodic domains
% Uses the function 'bwlabel'
%
% 
% Inputs: 	R 	- binary matrix
%
% Outputs: 	Ncells 	- number of cells   				      (size = [1,1])
% 		A	- area of each cell 				      (size = [length(Ncells),1])
%		L	- matrix with each unique cell labelled by an integer (size = size(R))
%		C	- the cell labels				      (size = [length(Ncells),1])


%% Expand R such that edges match
R = [R R(:,1)];
R = [R; R(1,:)];

%% Find distinct regions within matrix
L = bwlabel(R,8);

%% Make sure regions are distinct given doubly periodic geometery
for i = 1:size(R,2)

    if L(1,i) > 0 && L(end,i) > 0
    
        L(L==L(1,i) | L==L(end,i)) = min(L(1,i),L(end,i));
    end

end

for i = 1:size(R,1)

    if L(i,1) > 0 && L(i,end) > 0
        L(L==L(i,1) | L==L(i,end)) = min(L(i,1),L(i,end));
    end

end

%% Find number of rain cells
L = L(1:end-1,1:end-1);
R = R(1:end-1,1:end-1);
C = unique(L);
Ncells = length(C)-1;

%% Calculate cell area
cell_area = zeros(Ncells,1);
for i = 1:length(C)-1
    cell_area(i) = sum(L(:)==C(i+1));
end


if nargout > 2; varargout{1} = L; end;
if nargout > 3; varargout{2} = C; end;

 
end
 
 
    
 
