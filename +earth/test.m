% Run some simple tests to check things are okay

lon = 1:359;
lat = -89:89;

%% Make axis matrices
lon_mat = repmat(lon(:),[1 length(lat)]);
lat_mat = repmat(lat(:)',[length(lon) 1]);

s = lat_mat.^2;
t = sind(lon_mat);
u = exp(-(lon_mat-20).^2./(2.*20.^2) - (lat_mat+10).^2./(2.*10.^2) );

[dsx,dsy] = earth.grad(s,lon,lat);
[dtx,dty] = earth.grad(t,lon,lat);
[dux,duy] = earth.grad(u,lon,lat);

divtu = earth.div(t,u,lon,lat);



