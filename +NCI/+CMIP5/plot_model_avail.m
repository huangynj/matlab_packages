

c = NCI.CMIP5.CMIP5_metadata;

this_dir = fileparts(mfilename('fullpath'));

%% Find out the simulations, models, and variables we have data for at daily and monthly resolution

month_avail = zeros(length(c.models),length(c.simulations),length(c.variables));
day_avail = zeros(length(c.models),length(c.simulations),length(c.variables));
sixhr_avail = zeros(length(c.models),length(c.simulations),length(c.variables));
threehr_avail = zeros(length(c.models),length(c.simulations),length(c.variables));

month_avail_modlev = zeros(length(c.models),length(c.simulations),length(c.variables));
day_avail_modlev = zeros(length(c.models),length(c.simulations),length(c.variables));
sixhr_avail_modlev = zeros(length(c.models),length(c.simulations),length(c.variables));
threehr_avail_modlev = zeros(length(c.models),length(c.simulations),length(c.variables));

for mm = 1:length(c.models)
  disp(c.models{mm})

 try 
  model = NCI.CMIP5.model_info(c.models{mm});
  for ss = 1:length(c.simulations)
  simulation = c.simulations{ss};

    for vv = 1:length(c.variables)
      var = c.variables{vv};
    
      % Do we have data for this variable?
      try; day_avail(mm,ss,vv)   = model.(simulation).day.(var).num_times;         catch;  day_avail(mm,ss,vv)     = 0;     end
      try; month_avail(mm,ss,vv) = model.(simulation).mon.(var).num_times;         catch;  month_avail(mm,ss,vv)   = 0;     end
      try; sixhr_avail(mm,ss,vv) = model.(simulation).sixhr.(var).num_times;       catch;  sixhr_avail(mm,ss,vv)   = 0;     end
      try; threehr_avail(mm,ss,vv) = model.(simulation).threehr.(var).num_times;   catch;  threehr_avail(mm,ss,vv) = 0;     end

      % For certain variables we may also have data on native model levels
      try; day_avail_modlev(mm,ss,vv)     = model.(simulation).day.(var).num_times_modlev;       catch;  day_avail_modlev(mm,ss,vv)     = 0;     end
      try; month_avail_modlev(mm,ss,vv)   = model.(simulation).month.(var).num_times_modlev;     catch;  month_avail_modlev(mm,ss,vv)   = 0;     end
      try; sixhr_avail_modlev(mm,ss,vv)   = model.(simulation).sixhr.(var).num_times_modlev;     catch;  sixhr_avail_modlev(mm,ss,vv)   = 0;     end
      try; threehr_avail_modlev(mm,ss,vv)   = model.(simulation).threehr.(var).num_times_modlev; catch;  threehr_avail_modlev(mm,ss,vv) = 0;     end


      % Not really significant if 2-D variables are on model levels
      I = c.twod_vars>0;
      day_avail(:,:,I) = max(day_avail(:,:,I),day_avail_modlev(:,:,I));
      month_avail(:,:,I) = max(month_avail(:,:,I),month_avail_modlev(:,:,I));
      sixhr_avail(:,:,I) = max(sixhr_avail(:,:,I),sixhr_avail_modlev(:,:,I));
      threehr_avail(:,:,I) = max(threehr_avail(:,:,I),threehr_avail_modlev(:,:,I));

      day_avail_modlev(:,:,I) = 0;
      month_avail_modlev(:,:,I) = 0;
      sixhr_avail_modlev(:,:,I) = 0;
      threehr_avail_modlev(:,:,I) = 0;


    end
  end
 catch
 end

end

%% Now make a plot of it %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for kkk = 1:3

if kkk ==1
   the_data = day_avail;
   the_data_modlev = day_avail_modlev;
   the_string = 'Daily data, historical (green) and RCP85 (blue), on native model levels (red outline)';
   the_file = [this_dir '/file_info/daily_avail.pdf'];
elseif kkk==2
   the_data = month_avail;
   the_data_modlev = month_avail_modlev;
   the_string = 'Monthly data, historical (green) and RCP85 (blue)';
   the_file = [this_dir '/file_info/monthly_avail.pdf'];
else
   the_data = sixhr_avail;
   the_data_3hr = threehr_avail;
   the_data_modlev = sixhr_avail_modlev;
   the_string = 'Six-hourly data, historical (green) and RCP85 (blue), on model levs. (red squares), three hourly (red line)';
   the_file = [this_dir '/file_info/sixhr_avail.pdf'];

end

fig.bfig('a4p')


   annotation('textbox',[0.08 0.985 0.9 0.02],'string',the_string,'linestyle','none','fontsize',10);
for j = 1:length(c.variables)

  annotation('textbox',[0.19+(j-1)*0.035 0.97 0.0175 0.015],'string',c.variables{j},'linestyle','none','fontsize',7,'horizontalalignment','center');
  annotation('line',[0.18+(j-1)*0.035 0.18+(j-1)*0.035],[0.01 0.97],'linewidth',0.5)
  
end


for i = 1:length(c.models)

   annotation('textbox',[0.01 0.97-i*0.015 0.2 0.013],'string',num2str(i),'linestyle','none','fontsize',6);
   annotation('textbox',[0.05 0.97-i*0.015 0.2 0.013],'string',c.models{i},'linestyle','none','fontsize',6);
   annotation('line',[0.02 0.98],[0.9665-i*0.015 0.9665-i*0.015],'linewidth',0.5)

   for j = 1:length(c.variables)

     if the_data(i,1,j)>0
        annotation('rectangle',[0.19+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linestyle','none','facecolor','g')
     end

     if the_data_modlev(i,1,j)>0
        annotation('rectangle',[0.19+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linewidth',1,'color','r')
     end


     if the_data(i,2,j)>0
       annotation('rectangle',[0.202+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linestyle','none','facecolor','b')
     end

     if the_data_modlev(i,2,j)>0
        annotation('rectangle',[0.202+(j-1)*0.035 0.97-i*0.015 0.007 0.01],'linewidth',1,'color','r')
     end

     if kkk==3
        if the_data_3hr(i,1,j)>0
           annotation('ellipse',[0.19+(j-1)*0.035 0.97-i*0.015 0.002 0.01],'linewidth',1,'color','r')
        end
        if the_data_3hr(i,2,j)>0
           annotation('ellipse',[0.202+(j-1)*0.035 0.97-i*0.015 0.002 0.01],'linewidth',1,'color','r')
        end
     end

   end

end

print('-dpdf',the_file)
end





